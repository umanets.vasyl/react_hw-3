import React, { Component } from "react";
import "../Button/Button.scss";

function Button({ text, clickHandler, className }) {
  return (
    <button onClick={clickHandler} className={className}>
      {text}
    </button>
  );
}

export default Button;
