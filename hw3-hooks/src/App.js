import React, { useEffect, useState } from "react";
import Modal from "./components/Modal/Modal";
import "./App.scss";
import Router from "./routes";

function App() {
  const [products, setProducts] = useState([]);
  const [favorites, setFavorites] = useState([]);
  const [productsInCart, setProductsInCart] = useState([]);
  const [itemToBeAdded, setItemToBeAdded] = useState(null);
  const [itemToBeRemove, setItemToBeRemove] = useState(null);
  const [deleteCartModal, setDeleteCartModal] = useState(false);
  const [modalCart, setModalCart] = useState(false);

  const initStorageData = (products, itmprop) => {
    const storedInCart = JSON.parse(localStorage.getItem(itmprop)) || [];
    const productsArr = [];
    for (const id of storedInCart) {
      const product = products.find((i) => i.articul === id);
      productsArr.push(product);
    }
    switch (itmprop) {
      case "productsInCart":
        setProductsInCart([...productsInCart, ...productsArr]);
        break;
      case "favorites":
        setFavorites([...favorites, ...productsArr]);
        break;
    }
  };

  useEffect(() => {
    fetch("/products.json")
      .then((res) => res.json())
      .then((products) => {
        setProducts(products);
        initStorageData(products, "productsInCart");
        initStorageData(products, "favorites");
      });
  }, []);

  function addToFavourite(product) {
    let fav = [];
    let findElement = favorites.find(
      (elem) => elem.articul === product.articul
    );
    if (findElement) {
      fav = favorites.filter((elem) => elem.articul !== product.articul);
    } else {
      fav = [...favorites, product];
    }
    setFavorites(fav);
    localStorage.setItem(
      "favorites",
      JSON.stringify(fav.map((elem) => elem.articul))
    );
  }

  const addToCart = () => {
    const cartItem = [...productsInCart, itemToBeAdded];
    setProductsInCart(cartItem);
    setModalCart(false);
    localStorage.setItem(
      "productsInCart",
      JSON.stringify(cartItem.map((elem) => elem.articul))
    );
  };

  const openModalAddToCart = (product) => {
    setItemToBeAdded(product);
    setModalCart(true);
  };
  const openModalDeleteFromCart = (product) => {
    setItemToBeRemove(product);
    setDeleteCartModal(true);
  };

  const removeFromCart = () => {
    const filteredItems = productsInCart.filter(
      (i) => i.articul !== itemToBeRemove.articul
    );
    setProductsInCart(filteredItems);
    setDeleteCartModal(false);
    localStorage.setItem("productsInCart", JSON.stringify(filteredItems));
  };
  return (
    <>
      {deleteCartModal && (
        <Modal
          modalTitle={"Видалити товар"}
          modalText={"Чи хочете видалити товар з корзини?"}
          closeModal={setDeleteCartModal}
          submitFunction={removeFromCart}
        />
      )}

      {modalCart && (
        <Modal
          modalTitle={"Додати до корзини"}
          modalText={"Чи хочете додати товар до корзини?"}
          closeModal={setModalCart}
          submitFunction={addToCart}
        />
      )}

      <Router
        productsInCart={productsInCart}
        favorites={favorites}
        products={products}
        addToFavourite={addToFavourite}
        openModalAddToCart={openModalAddToCart}
        modalCart={modalCart}
        openModalDeleteFromCart={openModalDeleteFromCart}
      />
    </>
  );
}

export default App;
