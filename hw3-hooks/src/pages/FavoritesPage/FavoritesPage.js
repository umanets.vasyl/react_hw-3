import Products from "../../components/Products/Products";
import './FavoritesPage.scss'

const FavoritesPage = ({
  routeProducts,
  products,
  favorites,
  onAddFavFavourite,
  onAdd,
  modalCart,
}) => {
  return (
    <div className="container">
      {routeProducts.length > 0 ? (
        <>
          <h3 className="cart-container__title">
            В вибрані додано {routeProducts.length}{" "}
            {routeProducts.length > 4
              ? "товарів"
              : `${routeProducts.length > 1 ? "товари" : "товар"}`}
          </h3>
          <Products
            routeProducts={routeProducts}
            products={products}
            favorites={favorites}
            onAddFavourite={onAddFavFavourite}
            onAdd={onAdd}
            action={modalCart}
            text={"Додати до корзини"}
            btnClassName="card__footer-button"
          />
        </>
      ) : (
        <div className="empty">
          <h3>ВИБРАНИХ НЕМАЄ</h3>
        </div>
      )}
    </div>
  );
};

export default FavoritesPage;